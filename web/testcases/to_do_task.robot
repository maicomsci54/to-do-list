*** Settings ***
Resource     ../keywords/common/web_common.robot
Resource     ../keywords/to_do_list/to_do_list_keywords.robot
Test Setup   Open Browser With Option  ${ฺURL}  headless_mode=${False} 
Test Teardown   Clean Environment

*** Variable ***
${task_1}   task_1
${task_2}   task_2
${task_3}   task_3
${task_4}   task_4
${task_5}   task_5

*** Test Case ***
Manage To Do List 
    Add New Item List   ${task_1}   ${task_2}   ${task_3}  ${task_4}   ${task_5} 
    Click Change Tap  To-Do Tasks
    Verify To Do Task   ${task_1}   ${task_2}   ${task_3}  ${task_4}   ${task_5}  
    Delete To Do Task By Name  ${task_1}  
    Add To Do Task By Name  ${task_2}
    Add To Do Task By Name  ${task_3}
    Add To Do Task By Name  ${task_4}
    Click Change Tap  Completed
    Verify Completed List   ${task_2}  ${task_3}  ${task_4}
    Delete Completed By Name  ${task_3}
    Verify Completed List   ${task_2}  ${task_4}