*** Settings ***
Resource   ../../resources/locators/to_do_list/to_do_list_locators.robot

*** Keywords ***

Enter Name Of New Item 
  [Arguments]   ${item_name}
  Input Text   ${txb_add_item}   ${item_name}

Add New Item List
    [Arguments]   @{item_name_list}
    FOR    ${item_name}    IN    @{item_name_list}
    Enter Name Of New Item   ${item_name}
    Click New Item
    END

Click New Item 
  Click Element   ${btn_add} 

Verify To Do Task
    [Arguments]   @{item_name}
    @{item_list}=  Create List 
    ${elements}=    Get WebElements    ${lbl_ele_item}
    FOR    ${element}    IN    @{elements}
    ${text}=    Get Text    ${element}
    Append To List   ${item_list}  ${text}  
    END
    Lists Should Be Equal   ${item_list}   ${item_name}
  
Click Change Tap 
  [Arguments]   ${tap_name}
  ${lbl_menu_tap_locator}=  Generate Element From Dynamic Locator   ${lbl_menu_tap}   ${tap_name}
  Click Element   ${lbl_menu_tap_locator}

Delete To Do Task By Name 
  [Arguments]  ${item_name}
  ${btn_delete_locator}=  Generate Element From Dynamic Locator   ${btn_delete}   ${item_name}
  Click Element  ${btn_delete_locator}

Add To Do Task By Name
  [Arguments]  ${item_name}
  ${lbl_item_name_locator}=  Generate Element From Dynamic Locator   ${lbl_item_name}   ${item_name}
  Click Element  ${lbl_item_name_locator}

Delete Completed By Name
  [Arguments]  ${item_name}
  ${btn_complete_delete_locator}=  Generate Element From Dynamic Locator   ${btn_complete_delete}  ${item_name}
  Click Element  ${btn_complete_delete_locator}

Verify Completed List
    [Arguments]   @{item_name}
    @{item_list}=  Create List 
    ${elements}=    Get WebElements    ${ele_complete_item}
    FOR    ${element}    IN    @{elements}
    ${text}=    Get Text    ${element}
    ${text}=   Remove String Using Regexp   ${text}   [done]
    Append To List   ${item_list}  ${text}
    END
    Lists Should Be Equal   ${item_list}   ${item_name}