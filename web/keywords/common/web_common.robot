*** Settings ***
Resource    ../../resources/init.robot
Resource    ../../resources/locators/common/common_locators.robot

*** Variables ***
@{chrome_arguments}    --disable-infobars    --disable-gpu     --no-sandbox     --ignore-certificate-errors    --headless
${timeout}    30s

*** Keywords ***
Open Browser With Option
    [Arguments]    ${url}    ${browser}=Chrome    ${headless_mode}=${True}
    Run Keyword If    ${headless_mode} == ${True} and '${browser}' == 'Chrome'
    ...    Run Keywords
    ...    Open Browser With Chrome Headless Mode    ${url}
    ...    AND   Set Browser Implicit Wait    3s
    ...    ELSE IF    ${headless_mode} == ${False}
    ...    Run keywords   Open Browser    ${url}     ${browser}   options=add_argument("--ignore-certificate-errors")   
    ...    AND   Maximize Browser Window

Open Browser With Chrome Headless Mode
    [Arguments]      ${url}
    ${chrome_options}=    Set Chrome Options
    Open Browser    ${url}     ${browser}    options=${chrome_options}  
    Set Window Size    1920    1080

Set Chrome Options
    [Documentation]    Set Chrome options for headless mode
    ${options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver   
    FOR    ${option}    IN    @{chrome_arguments}
        Call Method    ${options}    add_argument    ${option}
    END
    [Return]    ${options} 

Clean Environment
    Close All Browsers